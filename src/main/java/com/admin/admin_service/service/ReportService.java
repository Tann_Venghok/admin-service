package com.admin.admin_service.service;

import com.admin.admin_service.model.PostResponse;
import com.admin.admin_service.model.ReportResponse;
import com.admin.admin_service.rest.message.BaseApiResponse;
import com.admin.admin_service.rest.message.BaseApiResponseWithPage;

import java.util.List;

public interface ReportService {
    BaseApiResponse<ReportResponse> getReportById(int id);
    BaseApiResponseWithPage<List<ReportResponse>> getAllReport();
    BaseApiResponseWithPage<List<ReportResponse>> getReportByPage(String pageNumber);
    BaseApiResponse<ReportResponse> deleteReportById(int id);
    BaseApiResponseWithPage<List<ReportResponse>> findReportByName(String caption);
    void getToken();
}
