package com.admin.admin_service.service.serviceImp;

import com.admin.admin_service.model.AnnouncementResponse;
import com.admin.admin_service.model.CompanyResponse;
import com.admin.admin_service.model.Token;
import com.admin.admin_service.model.User;
import com.admin.admin_service.rest.message.BaseApiResponse;
import com.admin.admin_service.rest.message.BaseApiResponseWithPagination;
import com.admin.admin_service.service.AdminService;
import org.modelmapper.ModelMapper;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

@Service
public class AdminServiceImp implements AdminService {


    private ModelMapper modelMapper = new ModelMapper();

    private RestTemplate restTemplate = new RestTemplate();

    HttpHeaders headers;

    HttpEntity<String> entity;

    Token token = new Token();

    @Override
    public int countAllCompany() {
        ParameterizedTypeReference<BaseApiResponse<Integer>> parameterizedTypeReference =
                new ParameterizedTypeReference<BaseApiResponse<Integer>>() {
                };
        headers = new HttpHeaders();
        getToken();
        System.out.println(token);
        headers.add("Authorization", "Bearer "+token.getAccess_token());
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

        entity = new HttpEntity<String>("parameters", headers);


        ResponseEntity<BaseApiResponse<Integer>> result = restTemplate.exchange("https://gateway.kshrd-ite.com/hr/companies/count", HttpMethod.GET, entity, parameterizedTypeReference);
        System.out.println(result.getBody());
        if (result.getBody().getData() == null) {
            return 0;
        }

        return result.getBody().getData();
    }

    @Override
    public int countAllAnnouncement() {
        return 0;
    }

    //TODO: Select company =========================================================
    @Override
    public List<CompanyResponse> selectCompany(String name) {

        ParameterizedTypeReference<BaseApiResponse<List<CompanyResponse>>> parameterizedTypeReference =
                new ParameterizedTypeReference<BaseApiResponse<List<CompanyResponse>>>() {
                };

        headers = new HttpHeaders();
        getToken();
        System.out.println(token);
        headers.add("Authorization", "Bearer "+token.getAccess_token());
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

        entity = new HttpEntity<String>("parameters", headers);

        ResponseEntity<BaseApiResponse<List<CompanyResponse>>> result = restTemplate.exchange("https://gateway.kshrd-ite.com/hr/companies?name="+name, HttpMethod.GET, entity, parameterizedTypeReference);
        if (result.getBody().getData() == null) {
            return null ;
        }

        return result.getBody().getData();
    }

    @Override
    public CompanyResponse selectOneCompany(int id) {

        ParameterizedTypeReference<BaseApiResponse<CompanyResponse>> parameterizedTypeReference =
                new ParameterizedTypeReference<BaseApiResponse<CompanyResponse>>() {
                };
        headers = new HttpHeaders();
        getToken();
        System.out.println(token);
        headers.add("Authorization", "Bearer "+token.getAccess_token());
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

        entity = new HttpEntity<String>("parameters", headers);


        ResponseEntity<BaseApiResponse<CompanyResponse>> result = restTemplate.exchange("https://gateway.kshrd-ite.com/hr/companies/"+id, HttpMethod.GET, entity, parameterizedTypeReference);
        if (result.getBody().getData() == null) {
            return null ;
        }
        return result.getBody().getData();
    }

    @Override
    public CompanyResponse banCompany(int id) {
        ParameterizedTypeReference<BaseApiResponse<CompanyResponse>> parameterizedTypeReference =
                new ParameterizedTypeReference<BaseApiResponse<CompanyResponse>>() {
                };
        headers = new HttpHeaders();
        getToken();
        System.out.println(token);
        headers.add("Authorization", "Bearer "+token.getAccess_token());
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

        entity = new HttpEntity<String>("parameters", headers);

        ResponseEntity<BaseApiResponse<CompanyResponse>> result = restTemplate.exchange("https://gateway.kshrd-ite.com/hr/companies/ban/"+id, HttpMethod.POST, entity, parameterizedTypeReference);
        if (result.getBody().getData() == null) {
            return null ;
        }
        return result.getBody().getData();
    }

    @Override
    public CompanyResponse unbanCompany(int id) {
        ParameterizedTypeReference<BaseApiResponse<CompanyResponse>> parameterizedTypeReference =
                new ParameterizedTypeReference<BaseApiResponse<CompanyResponse>>() {
                };
        headers = new HttpHeaders();
        getToken();
        System.out.println(token);
        headers.add("Authorization", "Bearer "+token.getAccess_token());
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

        entity = new HttpEntity<String>("parameters", headers);

        ResponseEntity<BaseApiResponse<CompanyResponse>> result = restTemplate.exchange("https://gateway.kshrd-ite.com/hr/companies/unban/"+id, HttpMethod.POST, entity, parameterizedTypeReference);
        if (result.getBody().getData() == null) {
            return null ;
        }
        return result.getBody().getData();
    }

    @Override
    public List<AnnouncementResponse> selectAnnouncementByCompany(int companyId) {
        ParameterizedTypeReference<BaseApiResponse<List<AnnouncementResponse>>> parameterizedTypeReference =
                new ParameterizedTypeReference<BaseApiResponse<List<AnnouncementResponse>>>() {
                };

        headers = new HttpHeaders();
        getToken();
        System.out.println(token);
        headers.add("Authorization", "Bearer "+token.getAccess_token());
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

        entity = new HttpEntity<String>("parameters", headers);
            ResponseEntity<BaseApiResponse<List<AnnouncementResponse>>> result = restTemplate.exchange("https://gateway.kshrd-ite.com/hr/announcements/company/"+companyId, HttpMethod.GET, entity, parameterizedTypeReference);
        if (result.getBody().getData() == null) {
            return null ;
        }

        return result.getBody().getData();
    }

    @Override
    public AnnouncementResponse selectOneAnnouncement(int id) {

        ParameterizedTypeReference<BaseApiResponse<AnnouncementResponse>> parameterizedTypeReference =
                new ParameterizedTypeReference<BaseApiResponse<AnnouncementResponse>>() {
                };

        headers = new HttpHeaders();
        getToken();
        System.out.println(token);
        headers.add("Authorization", "Bearer "+token.getAccess_token());
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

        entity = new HttpEntity<String>("parameters", headers);
        ResponseEntity<BaseApiResponse<AnnouncementResponse>> result = restTemplate.exchange("https://gateway.kshrd-ite.com/hr/announcements/"+id, HttpMethod.GET, entity, parameterizedTypeReference);
        if (result.getBody().getData() == null) {
            return null ;
        }
        return result.getBody().getData();
    }

    @Override
    public AnnouncementResponse banAnnouncement(int id) {
        ParameterizedTypeReference<BaseApiResponse<AnnouncementResponse>> parameterizedTypeReference =
                new ParameterizedTypeReference<BaseApiResponse<AnnouncementResponse>>() {
                };

        headers = new HttpHeaders();
        getToken();
        System.out.println(token);
        headers.add("Authorization", "Bearer "+token.getAccess_token());
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

        entity = new HttpEntity<String>("parameters", headers);
        ResponseEntity<BaseApiResponse<AnnouncementResponse>> result = restTemplate.exchange("https://gateway.kshrd-ite.com/hr/announcements/ban/"+id, HttpMethod.POST, entity, parameterizedTypeReference);
        if (result.getBody().getData() == null) {
            return null ;
        }
        return result.getBody().getData();
    }

    @Override
    public AnnouncementResponse unbanAnnouncement(int id) {
        ParameterizedTypeReference<BaseApiResponse<AnnouncementResponse>> parameterizedTypeReference =
                new ParameterizedTypeReference<BaseApiResponse<AnnouncementResponse>>() {
                };

        headers = new HttpHeaders();
        getToken();
        System.out.println(token);
        headers.add("Authorization", "Bearer "+token.getAccess_token());
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

        entity = new HttpEntity<String>("parameters", headers);
        ResponseEntity<BaseApiResponse<AnnouncementResponse>> result = restTemplate.exchange("https://gateway.kshrd-ite.com/hr/announcements/unban/"+id, HttpMethod.POST, entity, parameterizedTypeReference);
        if (result.getBody().getData() == null) {
            return null ;
        }
        return result.getBody().getData();
    }

    public void getToken(){
        RestTemplate restTemplate = new RestTemplate();

        String url = "https://gateway.kshrd-ite.com/root/post/oauth2/token";
        String clientId = "fIEZTLtFy3YutyKDcFadMeZ3cb5BouLP";
        String redirectUri = "http://35.197.132.204:31000/home";
        String username = "jobroom";
        String password = "jobroom";
        String clientSecret = "9bnm8sV0HwgcMx6pqk9x0WWR9tCzYyGi";
        String provisionKey = "AU3p0FF7yU4mA8thdUGG8xdpOV45Iwjb";
        String authenticated_userid = "connex";
        String scope = "email";

        HttpHeaders headers;

        HttpEntity<String> entity;

        headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        entity = new HttpEntity<String>("parameters", headers);

        token = restTemplate.postForObject(url,new User(),Token.class);

        System.out.println(token);

//        final String filepath= BaseAPI.TOKEN_PATH;
//
//        try {
//
//            FileOutputStream fileOut = new FileOutputStream(filepath);
//            ObjectOutputStream objectOut = new ObjectOutputStream(fileOut);
//            objectOut.writeObject(token);
//            objectOut.close();
//            System.out.println("The Object  was successfully written to a file");
//
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
    }

}