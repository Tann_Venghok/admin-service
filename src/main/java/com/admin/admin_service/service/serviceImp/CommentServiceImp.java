package com.admin.admin_service.service.serviceImp;

import com.admin.admin_service.model.CommentResponse;
import com.admin.admin_service.model.CommentUpdateStatus;
import com.admin.admin_service.model.Token;
import com.admin.admin_service.model.User;
import com.admin.admin_service.rest.message.BaseApiResponse;
import com.admin.admin_service.rest.message.BaseApiResponseWithPage;
import com.admin.admin_service.service.CommentService;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

@Service
public class CommentServiceImp implements CommentService {
    private RestTemplate restTemplate = new RestTemplate();
    HttpHeaders headers;
    HttpEntity<String> entity;
    Token token;

    @Override
    public BaseApiResponse<CommentResponse> getCommentById(int id) {
        ParameterizedTypeReference<BaseApiResponse<CommentResponse>> parameterizedTypeReference =
                new ParameterizedTypeReference<BaseApiResponse<CommentResponse>>() {
                };

        headers = new HttpHeaders();
        getToken();
        headers.add("Authorization", "Bearer "+token.getAccess_token());
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        entity = new HttpEntity<String>("parameters", headers);
        ResponseEntity<BaseApiResponse<CommentResponse>> result = restTemplate.exchange("https://gateway.kshrd-ite.com/post/comments/"+ String.valueOf(id), HttpMethod.GET, entity, parameterizedTypeReference);
        if (result.getBody().getData() == null) {
            return null ;
        }
        System.out.println("Single comment" + result.getBody());
        return result.getBody();
    }

    @Override
    public BaseApiResponseWithPage<List<CommentResponse>> getAllComment() {

            ParameterizedTypeReference<BaseApiResponseWithPage<List<CommentResponse>>> parameterizedTypeReference =
                    new ParameterizedTypeReference<BaseApiResponseWithPage<List<CommentResponse>>>() {
                    };
            getToken();
            headers = new HttpHeaders();
            headers.add("Authorization", "Bearer "+token.getAccess_token());
            headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
            headers.setContentType(MediaType.APPLICATION_JSON);
            entity = new HttpEntity<String>("parameters", headers);

            ResponseEntity<BaseApiResponseWithPage<List<CommentResponse>>> result = restTemplate.exchange("https://gateway.kshrd-ite.com/post/comments/all?size=5", HttpMethod.GET, entity, parameterizedTypeReference);
            if (result.getBody().getData() == null) {
                return null ;
            }
            return result.getBody();

    }


    @Override
    public BaseApiResponseWithPage<List<CommentResponse>> getCommentByPage(String pageNumber) {
        ParameterizedTypeReference<BaseApiResponseWithPage<List<CommentResponse>>> parameterizedTypeReference =
                new ParameterizedTypeReference<BaseApiResponseWithPage<List<CommentResponse>>>() {
                };
        getToken();
        headers = new HttpHeaders();
        headers.add("Authorization", "Bearer "+token.getAccess_token());
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        entity = new HttpEntity<String>("parameters", headers);
        ResponseEntity<BaseApiResponseWithPage<List<CommentResponse>>> result = restTemplate.exchange("https://gateway.kshrd-ite.com/post/comments/all?page={pageNumber}&size=5", HttpMethod.GET, entity, parameterizedTypeReference, pageNumber);

        if (result.getBody().getData() == null) {
            return null ;
        }

        return result.getBody();
    }

    @Override
    public BaseApiResponse<CommentResponse> deleteCommentById(int id) {
        ParameterizedTypeReference<BaseApiResponse<CommentResponse>> parameterizedTypeReference =
                new ParameterizedTypeReference<BaseApiResponse<CommentResponse>>() {
                };
        getToken();
        headers = new HttpHeaders();
        headers.add("Authorization", "Bearer "+token.getAccess_token());
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        entity = new HttpEntity<String>("parameters", headers);
        ResponseEntity<BaseApiResponse<CommentResponse>> result = restTemplate.exchange("https://gateway.kshrd-ite.com/post/comments/"+ String.valueOf(id), HttpMethod.DELETE, entity, parameterizedTypeReference);
        if (result.getBody().getData() == null) {
            return null ;
        }
        return result.getBody();
    }

    @Override
    public BaseApiResponse<CommentResponse> setCommentStatusToFalseById(int id) {
        ParameterizedTypeReference<BaseApiResponse<CommentResponse>> parameterizedTypeReference =
                new ParameterizedTypeReference<BaseApiResponse<CommentResponse>>() {
                };
        getToken();
        headers = new HttpHeaders();
        headers.add("Authorization", "Bearer "+token.getAccess_token());
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        CommentUpdateStatus commentUpdateStatus = new CommentUpdateStatus(false);
        HttpEntity<CommentUpdateStatus> requestUpdate = new HttpEntity<CommentUpdateStatus>(commentUpdateStatus, headers);
        ResponseEntity<BaseApiResponse<CommentResponse>> result = restTemplate.exchange("https://gateway.kshrd-ite.com/post/comments/status/"+ String.valueOf(id), HttpMethod.PUT, requestUpdate, parameterizedTypeReference);
        if (result.getBody().getData() == null) {
            return null ;
        }
        return result.getBody();
    }

    @Override
    public BaseApiResponse<CommentResponse> setCommentStatusToTrueById(int id) {
        ParameterizedTypeReference<BaseApiResponse<CommentResponse>> parameterizedTypeReference =
                new ParameterizedTypeReference<BaseApiResponse<CommentResponse>>() {
                };
        getToken();
        headers = new HttpHeaders();
        headers.add("Authorization", "Bearer "+token.getAccess_token());
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        CommentUpdateStatus commentUpdateStatus = new CommentUpdateStatus(true);
        HttpEntity<CommentUpdateStatus> requestUpdate = new HttpEntity<CommentUpdateStatus>(commentUpdateStatus, headers);
        ResponseEntity<BaseApiResponse<CommentResponse>> result = restTemplate.exchange("https://gateway.kshrd-ite.com/post/comments/status/"+ String.valueOf(id), HttpMethod.PUT, requestUpdate, parameterizedTypeReference);
        if (result.getBody().getData() == null) {
            return null ;
        }
        return result.getBody();
    }

    @Override
    public BaseApiResponseWithPage<List<CommentResponse>> findCommentByName(String name) {
        ParameterizedTypeReference<BaseApiResponseWithPage<List<CommentResponse>>> parameterizedTypeReference =
                new ParameterizedTypeReference<BaseApiResponseWithPage<List<CommentResponse>>>() {
                };
        getToken();
        headers = new HttpHeaders();
        headers.add("Authorization", "Bearer "+token.getAccess_token());
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        entity = new HttpEntity<String>("parameters", headers);
        ResponseEntity<BaseApiResponseWithPage<List<CommentResponse>>> result = restTemplate.exchange("https://gateway.kshrd-ite.com/post/comments/search?username={name}&size=5", HttpMethod.GET, entity, parameterizedTypeReference, name);

        if (result.getBody().getData() == null) {
            return null ;
        }
        return result.getBody();
    }

    @Override
    public void getToken() {
        RestTemplate restTemplate = new RestTemplate();

        String url = "https://gateway.kshrd-ite.com/root/post/oauth2/token";
        String clientId = "fIEZTLtFy3YutyKDcFadMeZ3cb5BouLP";
        String redirectUri = "http://35.197.132.204:31000/home";
        String username = "jobroom";
        String password = "jobroom";
        String clientSecret = "9bnm8sV0HwgcMx6pqk9x0WWR9tCzYyGi";
        String provisionKey = "AU3p0FF7yU4mA8thdUGG8xdpOV45Iwjb";
        String authenticated_userid = "connex";
        String scope = "email";

        HttpHeaders headers;

        HttpEntity<String> entity;

        headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        entity = new HttpEntity<String>("parameters", headers);

        token = restTemplate.postForObject(url,new User(), Token.class);

        System.out.println(token);
    }


}
