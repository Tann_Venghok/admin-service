package com.admin.admin_service.service.serviceImp;

import com.admin.admin_service.model.Feedback;
import com.admin.admin_service.model.Token;
import com.admin.admin_service.model.User;
import com.admin.admin_service.rest.message.BaseApiResponse;
import com.admin.admin_service.rest.message.BaseApiResponseWithPagination;
import com.admin.admin_service.service.FeedbackService;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

@Service
public class FeedbackServiceImp implements FeedbackService {
    private RestTemplate restTemplate=new RestTemplate();
    private String baseAPiAnnouncement="https://gateway.kshrd-ite.com/job/";
    Token token = new Token();
    @Override
    public ResponseEntity<BaseApiResponseWithPagination<List<Feedback>>> findAllFeedback(int page) {
        ParameterizedTypeReference<BaseApiResponseWithPagination<List<Feedback>>> parameterizedTypeReference =
                new ParameterizedTypeReference<BaseApiResponseWithPagination<List<Feedback>>>() {
                };

        HttpHeaders headers = new HttpHeaders();
        getToken();
        headers.add("Authorization", "Bearer "+token.getAccess_token());
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
        ResponseEntity<BaseApiResponseWithPagination<List<Feedback>>> result = restTemplate.exchange(baseAPiAnnouncement+"feedback?"+"page="+page, HttpMethod.GET, entity, parameterizedTypeReference);
        return result;
    }

    @Override
    public ResponseEntity<BaseApiResponse<Long>> countFeedback() {
        ParameterizedTypeReference<BaseApiResponse<Long>> parameterizedTypeReference =
                new ParameterizedTypeReference<BaseApiResponse<Long>>() {
                };

        HttpHeaders headers = new HttpHeaders();
        getToken();
        headers.add("Authorization", "Bearer "+token.getAccess_token());
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
        ResponseEntity<BaseApiResponse<Long>> result = restTemplate.exchange(baseAPiAnnouncement+"feedback/count", HttpMethod.GET, entity, parameterizedTypeReference);
        return result;
    }

    public void getToken() {
        RestTemplate restTemplate = new RestTemplate();

        String url = "https://gateway.kshrd-ite.com/root/post/oauth2/token";
        String clientId = "fIEZTLtFy3YutyKDcFadMeZ3cb5BouLP";
        String redirectUri = "http://35.197.132.204:31000/home";
        String username = "jobroom";
        String password = "jobroom";
        String clientSecret = "9bnm8sV0HwgcMx6pqk9x0WWR9tCzYyGi";
        String provisionKey = "AU3p0FF7yU4mA8thdUGG8xdpOV45Iwjb";
        String authenticated_userid = "connex";
        String scope = "email";

        HttpHeaders headers;

        HttpEntity<String> entity;

        headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        entity = new HttpEntity<String>("parameters", headers);

        token = restTemplate.postForObject(url,new User(), Token.class);

    }
}
