package com.admin.admin_service.service.serviceImp;

import com.admin.admin_service.model.PostResponse;
import com.admin.admin_service.model.ReportResponse;
import com.admin.admin_service.model.Token;
import com.admin.admin_service.model.User;
import com.admin.admin_service.rest.message.BaseApiResponse;
import com.admin.admin_service.rest.message.BaseApiResponseWithPage;
import com.admin.admin_service.service.ReportService;
import org.modelmapper.ModelMapper;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

@Service
public class ReportServiceImp implements ReportService {
    private ModelMapper modelMapper = new ModelMapper();
    private RestTemplate restTemplate = new RestTemplate();
    HttpHeaders headers;
    HttpEntity<String> entity;
    Token token;

    @Override
    public BaseApiResponse<ReportResponse> getReportById(int id) {
        ParameterizedTypeReference<BaseApiResponse<ReportResponse>> parameterizedTypeReference =
                new ParameterizedTypeReference<BaseApiResponse<ReportResponse>>() {
                };
        getToken();
        headers = new HttpHeaders();
        headers.add("Authorization", "Bearer "+token.getAccess_token());
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        entity = new HttpEntity<String>("parameters", headers);
        ResponseEntity<BaseApiResponse<ReportResponse>> result = restTemplate.exchange("https://gateway.kshrd-ite.com/post/reports/"+ String.valueOf(id), HttpMethod.GET, entity, parameterizedTypeReference);
        System.out.println("Report result get one "+ result.getBody());
        if (result.getBody().getData() == null) {
            return null ;
        }

        return result.getBody();
    }

    @Override
    public BaseApiResponseWithPage<List<ReportResponse>> getAllReport() {
        ParameterizedTypeReference<BaseApiResponseWithPage<List<ReportResponse>>> parameterizedTypeReference =
                new ParameterizedTypeReference<BaseApiResponseWithPage<List<ReportResponse>>>() {
                };
        getToken();
        headers = new HttpHeaders();
        headers.add("Authorization", "Bearer "+token.getAccess_token());
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        entity = new HttpEntity<String>("parameters", headers);
        ResponseEntity<BaseApiResponseWithPage<List<ReportResponse>>> result = restTemplate.exchange("https://gateway.kshrd-ite.com/post/reports?size=5", HttpMethod.GET, entity, parameterizedTypeReference);
        System.out.println("Report get all result "+ result.getBody());
        if (result.getBody().getData() == null) {
            return null ;
        }
        return result.getBody();
    }

    @Override
    public BaseApiResponseWithPage<List<ReportResponse>> getReportByPage(String pageNumber) {
        ParameterizedTypeReference<BaseApiResponseWithPage<List<ReportResponse>>> parameterizedTypeReference =
                new ParameterizedTypeReference<BaseApiResponseWithPage<List<ReportResponse>>>() {
                };
        getToken();
        headers = new HttpHeaders();
        headers.add("Authorization", "Bearer "+token.getAccess_token());
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        entity = new HttpEntity<String>("parameters", headers);
        ResponseEntity<BaseApiResponseWithPage<List<ReportResponse>>> result = restTemplate.exchange("https://gateway.kshrd-ite.com/post/reports?page={pageNumber}&size=5", HttpMethod.GET, entity, parameterizedTypeReference, pageNumber);
        System.out.println("Report result "+ result.getBody());
        if (result.getBody().getData() == null) {
            return null ;
        }
        return result.getBody();
    }

    @Override
    public BaseApiResponse<ReportResponse> deleteReportById(int id) {
        ParameterizedTypeReference<BaseApiResponse<ReportResponse>> parameterizedTypeReference =
                new ParameterizedTypeReference<BaseApiResponse<ReportResponse>>() {
                };
        getToken();
        headers = new HttpHeaders();
        headers.add("Authorization", "Bearer "+token.getAccess_token());
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        entity = new HttpEntity<String>("parameters", headers);
        ResponseEntity<BaseApiResponse<ReportResponse>> result = restTemplate.exchange("https://gateway.kshrd-ite.com/post/reports/"+ String.valueOf(id), HttpMethod.DELETE, entity, parameterizedTypeReference);
        if (result.getBody().getData() == null) {
            return null ;
        }
        return result.getBody();
    }

    @Override
    public BaseApiResponseWithPage<List<ReportResponse>> findReportByName(String caption) {
        return null;
    }

    @Override
    public void getToken() {
        RestTemplate restTemplate = new RestTemplate();

        String url = "https://gateway.kshrd-ite.com/root/post/oauth2/token";

        HttpHeaders headers;

        HttpEntity<String> entity;

        headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        entity = new HttpEntity<String>("parameters", headers);

        token = restTemplate.postForObject(url,new User(), Token.class);
    }
}
