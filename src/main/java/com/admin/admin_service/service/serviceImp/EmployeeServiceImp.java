package com.admin.admin_service.service.serviceImp;

import com.admin.admin_service.model.*;
import com.admin.admin_service.rest.message.BaseApiResponse;
import com.admin.admin_service.rest.message.BaseApiResponseWithPage;
import com.admin.admin_service.service.EmployeeService;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

@Service
public class EmployeeServiceImp implements EmployeeService {
    private RestTemplate restTemplate = new RestTemplate();
    HttpHeaders headers;
    HttpEntity<String> entity;
    Token token;

    @Override
    public BaseApiResponse<EmployeeResponse> getEmployeeById(int id) {
        ParameterizedTypeReference<BaseApiResponse<EmployeeResponse>> parameterizedTypeReference =
                new ParameterizedTypeReference<BaseApiResponse<EmployeeResponse>>() {
                };
        getToken();
        headers = new HttpHeaders();
        headers.add("Authorization", "Bearer "+token.getAccess_token());
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        entity = new HttpEntity<String>("parameters", headers);
        ResponseEntity<BaseApiResponse<EmployeeResponse>> result = restTemplate.exchange("https://gateway.kshrd-ite.com/employee/employees/"+ String.valueOf(id), HttpMethod.GET, entity, parameterizedTypeReference);
        if (result.getBody().getData() == null) {
            return null ;
        }

        return result.getBody();
    }

    @Override
    public BaseApiResponseWithPage<List<EmployeeResponse>> getAllEmployee() {
        ParameterizedTypeReference<BaseApiResponseWithPage<List<EmployeeResponse>>> parameterizedTypeReference =
                new ParameterizedTypeReference<BaseApiResponseWithPage<List<EmployeeResponse>>>() {
                };
        getToken();
        headers = new HttpHeaders();
        headers.add("Authorization", "Bearer "+token.getAccess_token());
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.setContentType(MediaType.APPLICATION_JSON);
        entity = new HttpEntity<String>("parameters", headers);

        ResponseEntity<BaseApiResponseWithPage<List<EmployeeResponse>>> result = restTemplate.exchange("https://gateway.kshrd-ite.com/employee/employees/all?size=5", HttpMethod.GET, entity, parameterizedTypeReference);

        if (result.getBody().getData() == null) {
            return null ;
        }
        return result.getBody();
    }

    @Override
    public BaseApiResponseWithPage<List<EmployeeResponse>> getEmployeeByPage(String pageNumber) {
        ParameterizedTypeReference<BaseApiResponseWithPage<List<EmployeeResponse>>> parameterizedTypeReference =
                new ParameterizedTypeReference<BaseApiResponseWithPage<List<EmployeeResponse>>>() {
                };
        getToken();
        headers = new HttpHeaders();
        headers.add("Authorization", "Bearer "+token.getAccess_token());
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.setContentType(MediaType.APPLICATION_JSON);
        entity = new HttpEntity<String>("parameters", headers);

        ResponseEntity<BaseApiResponseWithPage<List<EmployeeResponse>>> result = restTemplate.exchange("https://gateway.kshrd-ite.com/employee/employees/all?page={pageNumber}&size=5", HttpMethod.GET, entity, parameterizedTypeReference, pageNumber);

        if (result.getBody().getData() == null) {
            return null ;
        }
        return result.getBody();
    }

    @Override
    public BaseApiResponse<EmployeeResponse> deleteEmployeeById(int id) {
        return null;
    }

    @Override
    public BaseApiResponse<EmployeeResponse> setEmployeeStatusToFalseById(int id) {
        ParameterizedTypeReference<BaseApiResponse<EmployeeResponse>> parameterizedTypeReference =
                new ParameterizedTypeReference<BaseApiResponse<EmployeeResponse>>() {
                };
        getToken();
        headers = new HttpHeaders();
        headers.add("Authorization", "Bearer "+token.getAccess_token());
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        EmployeeUpdateStatus employeeUpdateStatus = new EmployeeUpdateStatus(false);
        HttpEntity<EmployeeUpdateStatus> requestUpdate = new HttpEntity<EmployeeUpdateStatus>(employeeUpdateStatus, headers);
        ResponseEntity<BaseApiResponse<EmployeeResponse>> result = restTemplate.exchange("https://gateway.kshrd-ite.com/employee/employees/status/"+ String.valueOf(id), HttpMethod.PUT, requestUpdate, parameterizedTypeReference);
        if (result.getBody().getData() == null) {
            return null ;
        }
        return result.getBody();
    }

    @Override
    public BaseApiResponse<EmployeeResponse> setEmployeeStatusToTrueById(int id) {
        ParameterizedTypeReference<BaseApiResponse<EmployeeResponse>> parameterizedTypeReference =
                new ParameterizedTypeReference<BaseApiResponse<EmployeeResponse>>() {
                };
        getToken();
        headers = new HttpHeaders();
        headers.add("Authorization", "Bearer "+token.getAccess_token());
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        EmployeeUpdateStatus employeeUpdateStatus = new EmployeeUpdateStatus(true);
        HttpEntity<EmployeeUpdateStatus> requestUpdate = new HttpEntity<EmployeeUpdateStatus>(employeeUpdateStatus, headers);
        ResponseEntity<BaseApiResponse<EmployeeResponse>> result = restTemplate.exchange("https://gateway.kshrd-ite.com/employee/employees/status/"+ String.valueOf(id), HttpMethod.PUT, requestUpdate, parameterizedTypeReference);
        if (result.getBody().getData() == null) {
            return null ;
        }
        return result.getBody();
    }

    @Override
    public BaseApiResponseWithPage<List<EmployeeResponse>> findEmployeeByName(String name) {
        ParameterizedTypeReference<BaseApiResponseWithPage<List<EmployeeResponse>>> parameterizedTypeReference =
                new ParameterizedTypeReference<BaseApiResponseWithPage<List<EmployeeResponse>>>() {
                };
        getToken();
        headers = new HttpHeaders();
        headers.add("Authorization", "Bearer "+token.getAccess_token());
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        entity = new HttpEntity<String>("parameters", headers);
        ResponseEntity<BaseApiResponseWithPage<List<EmployeeResponse>>> result = restTemplate.exchange("https://gateway.kshrd-ite.com/employee/search/employees?name={name}&size=5", HttpMethod.GET, entity, parameterizedTypeReference, name);

        if (result.getBody().getData() == null) {
            return null ;
        }
        return result.getBody();
    }

    @Override
    public void getToken() {
        RestTemplate restTemplate = new RestTemplate();

        String url = "https://gateway.kshrd-ite.com/root/post/oauth2/token";
        String clientId = "fIEZTLtFy3YutyKDcFadMeZ3cb5BouLP";
        String redirectUri = "http://35.197.132.204:31000/home";
        String username = "jobroom";
        String password = "jobroom";
        String clientSecret = "9bnm8sV0HwgcMx6pqk9x0WWR9tCzYyGi";
        String provisionKey = "AU3p0FF7yU4mA8thdUGG8xdpOV45Iwjb";
        String authenticated_userid = "connex";
        String scope = "email";

        HttpHeaders headers;

        HttpEntity<String> entity;

        headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        entity = new HttpEntity<String>("parameters", headers);

        token = restTemplate.postForObject(url,new User(), Token.class);

        System.out.println(token);

    }
}
