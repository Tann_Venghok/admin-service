package com.admin.admin_service.model;

public class ReportResponse {
    private int id;
    private UserResponse userResponse;
    private String description;
    private Post post;

    public ReportResponse() {
    }

    public ReportResponse(int id, UserResponse userResponse, String description, Post post) {
        this.id = id;
        this.userResponse = userResponse;
        this.description = description;
        this.post = post;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public UserResponse getUserResponse() {
        return userResponse;
    }

    public void setUserResponse(UserResponse userResponse) {
        this.userResponse = userResponse;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }

    @Override
    public String toString() {
        return "ReportResponse{" +
                "id=" + id +
                ", userResponse=" + userResponse +
                ", description='" + description + '\'' +
                ", post=" + post +
                '}';
    }
}
